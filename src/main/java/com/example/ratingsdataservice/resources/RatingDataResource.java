package com.example.ratingsdataservice.resources;

import com.example.ratingsdataservice.dto.UserRatingDto;
import com.example.ratingsdataservice.modal.Rating;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/ratingsdata")
public class RatingDataResource {
    @RequestMapping("/{movieId}")
    public Rating getRating(@PathVariable("movieId") String movieId){
        return new Rating(movieId,4);
    }

    @RequestMapping("/user/{userId}")
    public UserRatingDto getUserRating(@PathVariable("userId") String userId){
        List<Rating> ratings = Arrays.asList(
                new Rating( "2",4),
                new Rating("3",1)
        );

        UserRatingDto userRatingDto = new UserRatingDto();
        userRatingDto.setUserRating(ratings);
        return userRatingDto;
    }
}
