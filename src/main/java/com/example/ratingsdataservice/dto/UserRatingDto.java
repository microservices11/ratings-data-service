package com.example.ratingsdataservice.dto;

import com.example.ratingsdataservice.modal.Rating;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class UserRatingDto {
    private List<Rating> userRating;
}

